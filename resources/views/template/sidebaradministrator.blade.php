 <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li @click="menu=1" class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-user"></i>
                <p> Clientes</p>
            </a>
        </li>
        <li @click="menu=2" class="nav-item">
            <a href="#" class="nav-link">
                <i class="fas fa-bus nav-icon"></i>
                <p>Concesionarios</p>
            </a>
        </li>
        <li @click="menu=3" class="nav-item">
            <a href="#" class="nav-link">
                <i class="fas fa-users-cog nav-icon"></i>
                <p>Usuarios</p>
            </a>
        </li>
    </ul>
</nav>
     