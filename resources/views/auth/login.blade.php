@extends('auth.content')

@section('login')
<div class="login-box">
    <div class="login-logo">
      <a href="#"><b>Automotriz</b>GEOR</a>
    </div>
    
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Ingresa tus datos de acceso</p>
        
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="input-group mb-3  ">
                <input type="text" name="email" @error('email') is-invalid @enderror value="{{old('email')}}" id="email" class="form-control" placeholder="Correo electrónico" required autocomplete="email" autofocus>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>
                
            </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <div class="input-group mb-3 ">
                <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"  placeholder="Contraseña">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
            </div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <div class="row">
                <div class="col-8">
                  <div class="icheck-primary">
                    <input type="checkbox" id="remember">
                    <label for="remember">
                      Recordar
                    </label>
                  </div>
                </div>

                <div class="col-4">
                  <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                </div>
            </div>
       </form>
      </div>
    </div>
</div>
@endsection
