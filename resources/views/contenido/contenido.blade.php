@extends('principal')

@section('contenido')
    @if(Auth::check())
        @if(Auth::user()->role_id == 1)
            <template v-if="menu==1">
                <clients></clients>
            </template>
            <template v-if="menu==2">
                <concessionaires></concessionaires>
            </template>
            <template v-if="menu==3">
                <users></users>
            </template>
        @elseif(Auth::user()->role_id == 2)
            <template v-if="menu==1">
                <clients></clients>
            </template>
        @endIf
    @endIf  
@endsection