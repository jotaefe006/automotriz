require('./bootstrap');

window.Vue = require('vue');


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('clients', require('./components/ClientComponent.vue').default);
Vue.component('concessionaires', require('./components/ConcessionaireComponent.vue').default);
Vue.component('users', require('./components/UserComponent.vue').default);

const app = new Vue({
    el: '#app',
    data :{
        menu : 0,
    },
    created(){
      
    }
});
