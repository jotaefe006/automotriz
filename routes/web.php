<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => ['auth']], function () { 

    Route::get('/home', function () { return view('contenido/contenido'); })->name('main');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    /**
     * Routes for Clients 
    **/
    Route::get('/client', 'ClientController@index');
    Route::post('/client/register', 'ClientController@store');
    Route::put('/client/update', 'ClientController@update');
    Route::put('/client/desactivate', 'ClientController@desactivate');
    Route::put('/client/activate', 'ClientController@activate');

    /**
     *Routes for Concessionaire 
     **/
    Route::get('/concessionaire/selectConcessionaire', 'ConcessionaireController@list');
    Route::get('/concessionaire', 'ConcessionaireController@index');
    Route::post('/concessionaire/register', 'ConcessionaireController@store');
    Route::put('/concessionaire/update', 'ConcessionaireController@update');

    /**
     * Routes for City 
     **/
    Route::get('/city', 'CityController@index');

    /**
     * Routes for Role
     **/
    Route::get('/role', 'RoleController@index');

    /**
     * Routes for User 
     **/
    Route::get('/user', 'UserController@index');
    Route::post('/user/register', 'UserController@store');
    Route::put('/user/update', 'UserController@update');

});