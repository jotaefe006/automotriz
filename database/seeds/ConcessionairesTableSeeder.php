<?php

use Illuminate\Database\Seeder;
use App\Concessionaire;
class ConcessionairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Concessionaire::truncate();

        $concessionaire = new Concessionaire;
        $concessionaire->city_id = '1';
        $concessionaire->name = 'BYD Auto del Perú';
        $concessionaire->save();

        $concessionaire = new Concessionaire;
        $concessionaire->city_id = '2';
        $concessionaire->name = 'AUTOLAND PERÚ';
        $concessionaire->save();

        $concessionaire = new Concessionaire;
        $concessionaire->city_id = '3';
        $concessionaire->name = 'One';
        $concessionaire->save();

        $concessionaire = new Concessionaire;
        $concessionaire->city_id = '4';
        $concessionaire->name = 'Fuso Perú';
        $concessionaire->save();

        $concessionaire = new Concessionaire;
        $concessionaire->city_id = '1';
        $concessionaire->name = 'Autos de Corea Perú';
        $concessionaire->save();

        $concessionaire = new Concessionaire;
        $concessionaire->city_id = '2';
        $concessionaire->name = 'Nissan Autoland';
        $concessionaire->save();

        Schema::enableForeignKeyConstraints();
    }
}
