<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        City::truncate();

        $city = new City;
        $city->name = 'Lima';
        $city->save();

        $city = new City;
        $city->name = 'Arequipa';
        $city->save();

        $city = new City;
        $city->name = 'Trujillo';
        $city->save();

        $city = new City;
        $city->name = 'Chiclayo';
        $city->save();

        $city = new City;
        $city->name = 'Piura';
        $city->save();

        $city = new City;
        $city->name = 'Huancayo';
        $city->save();

        $city = new City;
        $city->name = 'Cusco';
        $city->save();

        $city = new City;
        $city->name = 'Iquitos';
        $city->save();

        Schema::enableForeignKeyConstraints();
    }
}
