<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        Client::truncate();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Jhon Frey Diaz';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'mz 19 cs 4 los laureles';
        $client->phone = '3173187766';
        $client->email = 'jotaefe006@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Luis Carlos';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'El dorado';
        $client->phone = '72722552';
        $client->email = 'luiicakarlos@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Juan Diaz';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'Nueva Aranda';
        $client->phone = '7228418';
        $client->email = 'juancho@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Pedro Antonio Diaz';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'Villanueva';
        $client->phone = '3173187766';
        $client->email = 'pedroa@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Maria';
        $client->document_type = 'C.C';
        $client->document_number = '1085310418';
        $client->address = 'Los eliceos';
        $client->phone = '3173187766';
        $client->email = 'Mariasanta@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Luis fernando Ortiz';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'Valle de atriz';
        $client->phone = '7727253';
        $client->email = 'Luisefe@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '2';
        $client->name = 'Federico';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'Pandiaco';
        $client->phone = '31476626262';
        $client->email = 'federico@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '3';
        $client->name = 'Carlos';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'Altos de la colina';
        $client->phone = '38226262';
        $client->email = 'jotaefe006@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '4';
        $client->name = 'Julian';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'granada';
        $client->phone = '949725252';
        $client->email = 'juliancho006@gmail.com';
        $client->condition = '1';
        $client->save();

        $client = new Client;
        $client->concessionaire_id = '1';
        $client->name = 'Jorge';
        $client->document_type = 'C.C';
        $client->document_number = '1122783239';
        $client->address = 'Santa Monica';
        $client->phone = '319545736';
        $client->email = 'jorge@gmail.com';
        $client->condition = '1';
        $client->save();


        Schema::enableForeignKeyConstraints();
    }
}
