<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user = new User;
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->role_id = '1';
        $user->condition = '1';
        $user->password = bcrypt('admin');
        $user->save();

        $user = new User;
        $user->name = 'invitado';
        $user->email = 'invitado@invitado.com';
        $user->role_id = '2';
        $user->condition = '1';
        $user->password = bcrypt('invitado');
        $user->save();
    }
}
