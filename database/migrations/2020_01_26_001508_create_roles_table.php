<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',30)->unique();
            $table->string('description',30)->nullable();
            $table->boolean('condition')->default(1);
            $table->timestamps();
        });
        DB::table('roles')->insert(array('id'=>'1','name'=>'Administrador','description'=>'Administrador de area'));
        DB::table('roles')->insert(array('id'=>'2','name'=>'General','description'=>'Usuario Concesionario'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
