<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable =[
        'concessionaire_id','name', 'document_type', 'document_number', 'address', 'phone', 'condition', 'email'
    ];

    public function concessionaire(){
        return $this->belongsTo('App\Concessionaire');
    }
}
