<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concessionaire extends Model
{
    protected $fillable = ['city_id', 'name'];

    public function clients(){
        return  $this->hasMany('App\Client');
    }

    public function cities(){
        return $this->belongsTo('App\City');
    }
}
