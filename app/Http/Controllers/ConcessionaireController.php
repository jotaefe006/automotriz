<?php

namespace App\Http\Controllers;
use App\Concessionaire;
use Illuminate\Http\Request;

class ConcessionaireController extends Controller
{
    /**
     * Obtiene el listado de concesionarios registrado en la BD segun el filtro de busqueda
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //if(!$request->ajax()) return redirect('/');
 
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar=='' && $criterio ==''){
            $concessionaires= Concessionaire::join('cities','concessionaires.city_id', '=', 'cities.id')
            ->select(
                'concessionaires.id',
                'concessionaires.city_id',
                'cities.name as name_city',
                'concessionaires.name'
                
            )
            ->orderBy('concessionaires.id', 'desc')->paginate(10);
        }else if($buscar=='' && $criterio !=''){
            $concessionaires = Concessionaire::join('cities','concessionaires.city_id', '=', 'cities.id')
            ->select(
                'concessionaires.id',
                'concessionaires.city_id',
                'concessionaires.name',
                'cities.name as name_city'
                 )
            ->where('concessionaires.city_id', '=',  $criterio)
            ->orderBy('concessionaires.id', 'desc')->paginate(10);
        }else if($buscar!='' && $criterio ==''){
            $concessionaires = Concessionaire::join('cities','concessionaires.city_id', '=', 'cities.id')
            ->select(
                'concessionaires.id',
                'concessionaires.city_id',
                'concessionaires.name',
                'cities.name as name_city'
                )
            ->where('concessionaires.name', 'like', '%'. $buscar . '%')
            ->orderBy('concessionaires.id', 'desc')->paginate(10);
        }else if($buscar!='' && $criterio !=''){
            $concessionaires = Concessionaire::join('cities','concessionaires.city_id', '=', 'cities.id')
            ->select(
                'concessionaires.id',
                'concessionaires.city_id',
                'concessionaires.name',
                'cities.name as name_city' )
            ->where('concessionaires.name', 'like', '%'. $buscar . '%')
            ->where('concessionaires.city_id', '=',  $criterio)
            ->orderBy('concessionaires.id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total' => $concessionaires->total(),
                'current_page' => $concessionaires->currentPage(),
                'per_page' => $concessionaires->perPage(),
                'last_page' => $concessionaires->lastPage(),
                'from' => $concessionaires->firstItem(),
                'to' => $concessionaires->lastItem(),
            ],
            'concessionaires' => $concessionaires
        ];
    }

    /**
     * Obtiene un listado de los concesionarios registrados en la BD
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $concessionaires = Concessionaire::all();
        return ['concessionaires' => $concessionaires];
    }

   /**
     * Registra la información de un nuevo concesionario en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(!$request->ajax()) return redirect('/');

            $concessionaire = new Concessionaire();
            $concessionaire->city_id = $request->city_id;
            $concessionaire->name = $request->name;
            $concessionaire->save();
    }

    /**
     * Actualiza la información de  un concesionario registrado en la BD.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $concessionaire = Concessionaire::findOrFail($request->id);
            $concessionaire->city_id = $request->city_id;
            $concessionaire->name = $request->name;
            $concessionaire->save();
    }
}
