<?php

namespace App\Http\Controllers;
use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * index: Devuelve el listado de ciudades registradas en BD
     * @return \Illuminate\Http\Response cities
     **/
    public function index(){

        $cities = City::all();
        return ['cities' => $cities];
    }
}
