<?php

namespace App\Http\Controllers;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    /**
     * Devuelve el listado de roles registrados en BD
     * @return \Illuminate\Http\Response roles
     **/
    public function index(){

        $roles = Role::all();
        return ['roles' => $roles];
    }
}
