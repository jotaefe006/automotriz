<?php

namespace App\Http\Controllers;
use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    /**
     * Obtiene el listado de clientes registrados en la BD segun el filtro de busqueda
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
 
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar=='' && $criterio ==''){
            $clients= Client::join('concessionaires','clients.concessionaire_id', '=', 'concessionaires.id')
            ->select(
                'clients.id',
                'clients.concessionaire_id',
                'clients.name',
                'concessionaires.name as name_concessionaire',
                'clients.document_type',
                'clients.document_number',
                'clients.address',
                'clients.phone',
                'clients.email',
                'clients.condition' )
            ->orderBy('clients.id', 'desc')->paginate(10);
        }else if($buscar=='' && $criterio !=''){
            $clients = Client::join('concessionaires','clients.concessionaire_id', '=', 'concessionaires.id')
            ->select(
                'clients.id',
                'clients.concessionaire_id',
                'clients.name',
                'concessionaires.name as name_concessionaire',
                'clients.document_type',
                'clients.document_number',
                'clients.address',
                'clients.phone',
                'clients.email',
                'clients.condition' )
            ->where('clients.concessionaire_id', '=',  $criterio)
            ->orderBy('clients.id', 'desc')->paginate(10);
        }else if($buscar!='' && $criterio ==''){
            $clients = Client::join('concessionaires','clients.concessionaire_id', '=', 'concessionaires.id')
            ->select(
                'clients.id',
                'clients.concessionaire_id',
                'clients.name',
                'concessionaires.name as name_concessionaire',
                'clients.document_type',
                'clients.document_number',
                'clients.address',
                'clients.phone',
                'clients.email',
                'clients.condition' )
            ->where('clients.name', 'like', '%'. $buscar . '%')
            ->orderBy('clients.id', 'desc')->paginate(10);
        }else if($buscar!='' && $criterio !=''){
            $clients = Client::join('concessionaires','clients.concessionaire_id', '=', 'concessionaires.id')
            ->select(
                'clients.id',
                'clients.concessionaire_id',
                'clients.name',
                'concessionaires.name as name_concessionaire',
                'clients.document_type',
                'clients.document_number',
                'clients.address',
                'clients.phone',
                'clients.email',
                'clients.condition' )
            ->where('clients.name', 'like', '%'. $buscar . '%')
            ->where('clients.concessionaire_id', '=',  $criterio)
            ->orderBy('clients.id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total' => $clients->total(),
                'current_page' => $clients->currentPage(),
                'per_page' => $clients->perPage(),
                'last_page' => $clients->lastPage(),
                'from' => $clients->firstItem(),
                'to' => $clients->lastItem(),
            ],
            'clients' => $clients
        ];
    }

    /**
     * Registra la información de un nuevo cliente en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(!$request->ajax()) return redirect('/');

            $client = new Client();
            $client->concessionaire_id = $request->concessionaire_id;
            $client->name = $request->name;
            $client->document_type = $request->document_type;
            $client->document_number = $request->document_number;
            $client->address = $request->address;
            $client->phone = $request->phone;
            $client->email = $request->email;
            $client->condition = '1';
            $client->save();
    }
  
    /**
     * Actualiza la información de  un cliente registrado en la BD.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $client = Client::findOrFail($request->id);
            $client->concessionaire_id = $request->concessionaire_id;
            $client->name = $request->name;
            $client->document_type = $request->document_type;
            $client->document_number = $request->document_number;
            $client->address = $request->address;
            $client->phone = $request->phone;
            $client->email = $request->email;
            $client->condition = '1';
            $client->save();

    }

    /**
     * Eliminado lógico de un Cliente, se cambia a estado desactivado
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function desactivate(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = Client::findOrFail($request->id);
        $cliente->condition = '0';
        $cliente->save();
    }

     /**
     * activar  un Cliente, se cambia a estado activo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function activate(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $client = Client::findOrFail($request->id);
        $client->condition = '1';
        $client->save();
    }
}
