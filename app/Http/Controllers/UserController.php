<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Obtiene un listado de los usuarios registrados en la BD, según el filtro de busqueda
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //if(!$request->ajax()) return redirect('/');
 
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar=='' && $criterio ==''){
            $users= User::join('roles','users.role_id', '=', 'roles.id')
            ->select(
                'users.id',
                'users.role_id',
                'users.email',
                'roles.name as name_role',
                'users.name'
                
            )
            ->orderBy('users.id', 'desc')->paginate(10);
        }else if($buscar=='' && $criterio !=''){
            $users = User::join('roles','users.role_id', '=', 'roles.id')
            ->select(
                'users.id',
                'users.role_id',
                'users.email',
                'users.name',
                'roles.name as name_role'
                 )
            ->where('users.role_id', '=',  $criterio)
            ->orderBy('users.id', 'desc')->paginate(10);
        }else if($buscar!='' && $criterio ==''){
            $users = User::join('roles','users.role_id', '=', 'roles.id')
            ->select(
                'users.id',
                'users.role_id',
                'users.email',
                'users.name',
                'roles.name as name_role'
                )
            ->where('users.name', 'like', '%'. $buscar . '%')
            ->orderBy('users.id', 'desc')->paginate(10);
        }else if($buscar!='' && $criterio !=''){
            $users = User::join('roles','users.role_id', '=', 'roles.id')
            ->select(
                'users.id',
                'users.role_id',
                'users.email',
                'users.name',
                'roles.name as name_role' )
            ->where('users.name', 'like', '%'. $buscar . '%')
            ->where('users.role_id', '=',  $criterio)
            ->orderBy('users.id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total' => $users->total(),
                'current_page' => $users->currentPage(),
                'per_page' => $users->perPage(),
                'last_page' => $users->lastPage(),
                'from' => $users->firstItem(),
                'to' => $users->lastItem(),
            ],
            'users' => $users
        ];
    }

   /**
     * Registra la información de un nuevo usuario en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(!$request->ajax()) return redirect('/');

            $user = new User();
            $user->role_id = $request->role_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
    }

    /**
     * Actualiza la información de  un usuarioo registrado en la BD.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $user = User::findOrFail($request->id);
            $user->role_id = $request->role_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
    }
}
